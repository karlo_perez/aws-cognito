angular.module("App").service("awsService", awsService);
awsService.$inject = ["$q", "$localStorage"];
function awsService($q, $localStorage) {
  var user = null;
  var awsCredential = null;
  var cognitosync = new AWS.CognitoSync({ apiVersion: "2014-06-30" });
  var cognitoUser = null;

  this.init = function(credentials) {
    awsCredential = credentials;
  };

  this.login = function(username, password, awsCredential) {
    var getCategoryIsDone = false;
    var getProductIsDone = false;
    var getTransactionIsDone = false;

    var deferred = $q.defer();
    var poolData = {
      UserPoolId: awsCredential.userPoolId,
      ClientId: awsCredential.clientId
    };
    var userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData);

    var userData = {
      Username: username,
      Pool: userPool
    };

    var authenticationData = {
      Username: username,
      Password: password
    };
    var authenticationDetails = new AmazonCognitoIdentity.AuthenticationDetails(
      authenticationData
    );

    cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);
    cognitoUser.authenticateUser(authenticationDetails, {
      onSuccess: function(result) {
        AWS.config.region = awsCredential.region;
        var logins = {};
        logins[
          "cognito-idp." +
            awsCredential.region +
            ".amazonaws.com/" +
            awsCredential.userPoolId
        ] = result.getIdToken().getJwtToken();
        AWS.config.credentials = new AWS.CognitoIdentityCredentials({
          IdentityPoolId: awsCredential.identityPoolId,
          Logins: logins
        });

        console.log(AWS.config.credentials);

        var cognitosync = new AWS.CognitoSync({ apiVersion: "2014-06-30" });
        // get category

        cognitosync.listRecords(
          {
            DatasetName: "category" /* required */,
            IdentityId: AWS.config.credentials.identityId /* required */,
            IdentityPoolId: awsCredential.identityPoolId /* required */,
            LastSyncCount: 0,
            MaxResults: 100,
            NextToken: "0",
            SyncSessionToken: AWS.config.credentials.sessionToken
          },
          function(err, data) {
            if (err) {
              deferred.resolve('retry');
            } else {
              getCategoryIsDone = true;
              $localStorage.category = data.Records;

              if (
                getDataChecker(
                  getCategoryIsDone,
                  getProductIsDone,
                  getTransactionIsDone
                )
              )
                deferred.resolve();
            }
          }
        );

        // get product

        cognitosync.listRecords(
          {
            DatasetName: "product" /* required */,
            IdentityId: AWS.config.credentials.identityId /* required */,
            IdentityPoolId: awsCredential.identityPoolId /* required */,
            LastSyncCount: 0,
            MaxResults: 100,
            NextToken: "0",
            SyncSessionToken: AWS.config.credentials.sessionToken
          },
          function(err, data) {
            if (err) {
              deferred.resolve('retry');
            } else {
              getProductIsDone = true;
              $localStorage.product = data.Records;

              if (
                getDataChecker(
                  getCategoryIsDone,
                  getProductIsDone,
                  getTransactionIsDone
                )
              )
                deferred.resolve();
            }
          }
        );

        // get transaction

        cognitosync.listRecords(
          {
            DatasetName: "transaction" /* required */,
            IdentityId: AWS.config.credentials.identityId /* required */,
            IdentityPoolId: awsCredential.identityPoolId /* required */,
            LastSyncCount: 0,
            MaxResults: 100,
            NextToken: "0",
            SyncSessionToken: AWS.config.credentials.sessionToken
          },
          function(err, data) {
            if (err) {
              deferred.resolve('retry');
            } else {
              getTransactionIsDone = true;
              $localStorage.transaction = data.Records;

              if (
                getDataChecker(
                  getCategoryIsDone,
                  getProductIsDone,
                  getTransactionIsDone
                )
              )
                deferred.resolve();
            }
          }
        );
      },
      onFailure: function(err) {
        deferred.reject();
      },
      mfaRequired: function(codeDeliveryDetails) {
        deferred.reject();
      }
    });
    return deferred.promise;
  };

  this.logout = function() {
    if (cognitoUser != null) {
      cognitoUser.signOut();
    }
  };

  this.getRecords = function(dataSetName, nextToken, maxResults) {
    var deferred = $q.defer();
    var params = {
      DatasetName: dataSetName /* required */,
      IdentityId: AWS.config.credentials.identityId /* required */,
      IdentityPoolId: awsCredential.identityPoolId /* required */,
      LastSyncCount: 0,
      MaxResults: maxResults,
      NextToken: nextToken,
      SyncSessionToken: AWS.config.credentials.sessionToken
    };
    cognitosync.listRecords(params, function(err, data) {
      console.log(err, data);
      if (err) {
        deferred.reject();
      } else {
        deferred.resolve(data.Records);
      }
    });
    return deferred.promise;
  };

  function getDataChecker(param1, param2, param3) {
    if (param1 == true && param2 == true && param3 == true) return true;
    else return false;
  }
}
