angular
    .module('App')
    .directive('dbheader', dbheader);
function dbheader(){
    return {
        restrict: 'E',
        templateUrl: 'app/components/directives/views/dbheader.html',
        controller: 'dbheaderCtrl'
    };
}