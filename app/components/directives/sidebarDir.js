angular
    .module('App')
    .directive('sidebar', sidebar);
function sidebar(){
    return {
        restrict: 'E',
        templateUrl: 'app/components/directives/views/sidebar.html',
        controller: 'sidebarCtrl'
    };
}