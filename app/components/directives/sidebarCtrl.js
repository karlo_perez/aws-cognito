angular
    .module("App")
    .controller("sidebarCtrl", sidebarCtrl)
sidebarCtrl.$inject = ['$scope', '$state','$localStorage','$stateParams','awsService','$rootScope'];
function sidebarCtrl($scope, $state, $localStorage,$stateParams, awsService) {
    $scope.logout = function () {
        awsService.logout();
        $localStorage.user = null;
        $localStorage.category = null;
        $localStorage.product = null;
        $localStorage.transaction = null;
        $state.transitionTo("login");
    }

    $scope.currentState = $state.current.name;
    
}