angular
    .module("App")
    .controller("dbheaderCtrl", dbheaderCtrl)
    dbheaderCtrl.$inject = ['$scope', '$state','$localStorage','$stateParams'];
function dbheaderCtrl($scope, $state, $localStorage,$stateParams) {
    $scope.headerTitle = "";
    $scope.sideNavWidth = {};
    $scope.margin = {};
    var isOpen = false;

   
    switch($state.current.name) {
        case "home":
            $scope.headerTitle = "Category";
            break;
        case "product":
            $scope.headerTitle = "Product";
            break;
        default:
            $scope.headerTitle = "Transaction";
    }

    
    $scope.openNav = function () {
        if (isOpen) {
            $scope.sideNavWidth = {
                width: '0'
            }
            $scope.margin = {
                'margin-left': '0'
            }
            isOpen = false;
        } else {
            $scope.sideNavWidth = {
                width: '200px'
            }
            $scope.margin = {
                'margin-left': '192px'
            }
            isOpen = true;
        }

    }
}