angular
    .module('App')
    .config(configure);

configure.configure = ['$stateProvider', '$urlRouterProvider'];
function configure ($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('home', {
            url: '/category',
            templateUrl: 'app/components/views/homeView.html',
            controller: 'homeCtrl'
        })
        .state('product', {
            url: '/product',
            templateUrl: 'app/components/views/homeView.html',
            controller: 'homeCtrl'
        })
        .state('transaction', {
            url: '/transaction',
            templateUrl: 'app/components/views/homeView.html',
            controller: 'homeCtrl'
        })
        .state('login', {
            url: '/login',
            templateUrl: 'app/components/views/loginView.html',
            controller: 'loginCtrl'
        })
        

    $urlRouterProvider.otherwise("/login");
}