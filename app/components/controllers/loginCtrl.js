angular.module("App").controller("loginCtrl", loginCtrl);
loginCtrl.$inject = ["$scope", "$state", "awsService", "$localStorage"];
function loginCtrl($scope, $state, awsService, $localStorage) {
  $scope.isLoggingIn = false;
  $scope.username = "";
  $scope.password = "";
  $scope.isInvalidCredentials = false;
  $scope.selected = "Category";



  var awsCredential = {
    region: "us-west-2",
    userPoolId: "us-west-2_DeXydhjuy",
    clientId: "4lurtke1judgdmql50clkmeelb",
    identityPoolId: "us-west-2:100c7668-1498-4ece-b86f-935440bbb94e"
  };

  // init awsService
  awsService.init(awsCredential);

  $scope.login = function() {
    $scope.isLoggingIn = true;
    $scope.isInvalidCredentials = false;

    awsService.login($scope.username.toUpperCase(), $scope.password, awsCredential).then(
      function(data) {
        $localStorage.user = $scope.username;
        $state.transitionTo("home");
      },
      function(data) {
        if(data == "retry"){
          $scope.login();
        }else{
          $scope.isInvalidCredentials = true;
          $scope.isLoggingIn = false;
          $scope.$apply();
        }
        
      }
    );
  };
}
