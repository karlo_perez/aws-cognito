angular.module("App").controller("homeCtrl", homeCtrl);
homeCtrl.$inject = [
  "$scope",
  "$state",
  "$localStorage",
  "awsService",
  "$stateParams"
];
function homeCtrl($scope, $state, $localStorage, awsService, $stateParams) {
  if (!$localStorage.user) {
    $state.transitionTo("login");
  } else {
    $scope.user = $localStorage.user;
    $scope.headers = [];

    $scope.tableRows = [];

    try {
      switch ($state.current.name) {
        case "home":
          $scope.headerTitle = "Category";
          $scope.headers = [
            "id",
            "LastModifiedDate",
            "name",
            "description",
            "hasProducts",
            "deleted"
          ];
          $scope.tableRows = generateTableData($localStorage.category);
          break;
        case "product":
          $scope.headerTitle = "Product";
          $scope.headers = [
            "id",
            "LastModifiedDate",
            "categoryId",
            "description",
            "actualPrice",
            "taxPercentage",
            "deleted"
          ];
          $scope.tableRows = generateTableData($localStorage.product);
          break;
        default:
          $scope.headerTitle = "Transaction";
          $scope.headers = [
            "transactionId",
            "transactionStartTime",
            "transactionEndTime",
            "transactionType",
            "amountDue",
            "amountPaid",
            "changeDue",
            "sendEmail",
            "totalAmount",
            "totalItems",
            "totalProducts",
            "totalTax"
          ];
          $scope.tableRows = generateTableData($localStorage.transaction, true);
      }
    } catch (e) {
  
      $state.transitionTo("login");
      alert("something went wrong login again")
    }

    function generateTableData(rawData, isTransaction) {
      var data = [];
      var counter = 0;


      for(var i= 0; i < rawData.length; i++){
        var value = rawData[i];
        try {
          var row = [$scope.headers.length];

          if (!isTransaction)
            row[$scope.headers.indexOf("LastModifiedDate")] = new Date(
              value.LastModifiedDate
            ).toLocaleString();

          var obj = JSON.parse(value.Value);
          Object.keys(obj).forEach(function(k) {
            if (k.includes("Time")) {
              row[$scope.headers.indexOf(k)] = new Date(
                obj[k]
              ).toLocaleString();
            } else {
              row[$scope.headers.indexOf(k)] = obj[k];
            }
          });
          data.push(row);
        } catch (e) {
          continue;
        }
      }
      return data;
    }
  }
}
